#ifndef FIR_H_H
#define FIR_H_H

#include "filters.h"

void filter_fir_hs1_init(void);
void filter_fir_hs1_process(const float32_t *p_in_data, float32_t *p_out_data, uint32_t in_data_len);
void filter_fir_hs2_init(void);
void filter_fir_hs2_process(const float32_t* p_in_data, float32_t* p_out_data, uint32_t in_data_len);

#endif // FIR_H_H
