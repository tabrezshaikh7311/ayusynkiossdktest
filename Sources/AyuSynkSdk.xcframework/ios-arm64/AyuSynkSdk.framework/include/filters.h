#ifndef FILTERS_H
#define FILTERS_H

#ifdef __cplusplus
extern "C" {
#endif
	
#include <stdint.h>
#include <arm_math.h>
#include "filter_config.h"

extern float32_t a3b_max_amp;

typedef enum {
	SWITCH_MODE = 0,
	NO_FILTER = 1,
	HEART_FILTER = 2,
	LUNG_FILTER = 3,
} FILTER_mode;

uint32_t filter_process_blockA(int16_t *pcm_in);
void filter_process_blockB(int16_t *pcm_out);
void filter_set_mode(FILTER_mode mode);
FILTER_mode filter_get_mode(void);

extern float32_t f32_buff1[PCM_SAMPLES_IN];
extern float32_t f32_buff2[FILTER_BLOCK_C_SIZE];
extern float32_t f32_buff3[FILTER_BLOCK_C_SIZE];

#ifdef __cplusplus
}
#endif
	
#endif // FILTERS_H
