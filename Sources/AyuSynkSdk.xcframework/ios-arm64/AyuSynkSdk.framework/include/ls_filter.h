#ifndef LS_FILTER_H
#define LS_FILTER_H

#include <stdint.h>

void filter_ls_init(void);
uint32_t filter_ls_process_blockA(int16_t* pcm_in);
void filter_ls_process_blockB(int16_t *pcm_out);

#endif
