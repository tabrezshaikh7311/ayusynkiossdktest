// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.10 (swiftlang-5.10.0.13 clang-1500.3.9.4)
// swift-module-flags: -target arm64-apple-ios12.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -enable-bare-slash-regex -module-name AyuSynkSdk
import AVFoundation
@_exported import AyuSynkSdk
import CoreBluetooth
import CorePlot
import DeveloperToolsSupport
import Foundation
import Swift
import SwiftUI
import UIKit
import _Concurrency
import _StringProcessing
import _SwiftConcurrencyShims
public enum FilterType : Swift.Int {
  case NoFilter
  case HeartFilter
  case LungFilter
  public init?(rawValue: Swift.Int)
  public typealias RawValue = Swift.Int
  public var rawValue: Swift.Int {
    get
  }
}
public protocol DiagnosisReportUpdateListener {
  func reportRequestAdded(soundFile: AyuSynkSdk.SoundFile)
  func reportGenerated(soundFile: AyuSynkSdk.SoundFile)
  func onReportGenerationError(error: Swift.String?)
}
public enum SoundType {
  case HEART
  case LUNG
  case UNKNOWN
  public static func == (a: AyuSynkSdk.SoundType, b: AyuSynkSdk.SoundType) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public enum LocationType : Swift.String {
  case unknown
  public enum Heart : Swift.String {
    case aortic
    case pulmonic
    case tricuspid
    case mitral
    public init?(rawValue: Swift.String)
    public typealias RawValue = Swift.String
    public var rawValue: Swift.String {
      get
    }
  }
  public enum Lung : Swift.String {
    case anterior_upper_right
    case anterior_middle_right
    case anterior_lower_right
    case anterior_upper_left
    case anterior_middle_left
    case anterior_lower_left
    case lateral_upper_right
    case lateral_lower_right
    case lateral_upper_left
    case lateral_lower_left
    case posterior_upper_right
    case posterior_middle_right
    case posterior_lower_right
    case posterior_upper_left
    case posterior_middle_left
    case posterior_lower_left
    public init?(rawValue: Swift.String)
    public typealias RawValue = Swift.String
    public var rawValue: Swift.String {
      get
    }
  }
  public init?(rawValue: Swift.String)
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
}
public class SoundData {
  public init(fileUrl: Foundation.URL? = nil, fileLink: Swift.String? = nil, locationName: AyuSynkSdk.LocationType)
  public init(fileLink: Swift.String, locationName: AyuSynkSdk.LocationType.Heart)
  public init(fileLink: Swift.String, locationName: AyuSynkSdk.LocationType.Lung)
  public init(fileUrl: Foundation.URL, locationName: AyuSynkSdk.LocationType)
  public init(fileUrl: Foundation.URL, locationName: AyuSynkSdk.LocationType.Heart)
  public init(fileUrl: Foundation.URL, locationName: AyuSynkSdk.LocationType.Lung)
  @objc deinit
}
public class SoundFile {
  public init(soundData: AyuSynkSdk.SoundData)
  public init(soundDataList: [AyuSynkSdk.SoundData])
  public func getReports() -> [AyuSynkSdk.ReportData]?
  @objc deinit
}
public class HeartSoundData : AyuSynkSdk.SoundData {
  override public init(fileLink: Swift.String, locationName: AyuSynkSdk.LocationType.Heart)
  override public init(fileUrl: Foundation.URL, locationName: AyuSynkSdk.LocationType.Heart)
  @objc deinit
}
public class LungSoundData : AyuSynkSdk.SoundData {
  override public init(fileLink: Swift.String, locationName: AyuSynkSdk.LocationType.Lung)
  override public init(fileUrl: Foundation.URL, locationName: AyuSynkSdk.LocationType.Lung)
  @objc deinit
}
public struct ReportData {
  public let soundType: AyuSynkSdk.SoundType
  public let position: Swift.String?
  public let reportURL: Swift.String?
  public let heartBPM: Swift.String?
  public let heartConditionDetected: Swift.String?
  public let heartConditionConfidence: Swift.String?
}
extension Foundation.FileHandle {
  public func writeStr(_ val: Swift.String)
  public func writeUInt32(_ val: Swift.UInt32)
  public func writeUInt16(_ val: Swift.UInt16)
}
@_hasMissingDesignatedInitializers public class WaveFileDump {
  final public let fileLocation: Foundation.URL
  public func writeSample(sampleData: Foundation.Data)
  public func stopRecord()
  public func getFileLocation() -> Foundation.URL
  @objc deinit
}
public protocol RecorderListener : ObjectiveC.NSObjectProtocol {
  func elapsedTime(min: Swift.Int, secs: Swift.Int)
  func recordingComplete(recordID: Swift.String)
  func playingComplete()
}
@_hasMissingDesignatedInitializers public class BlueVoiceSyncQueue {
  public var mData: [Foundation.Data?]
  public var mPrevData: Foundation.Data?
  public func push(data: Foundation.Data)
  public func pop() -> Foundation.Data?
  @objc deinit
}
public enum PlayBackSpeeds : Swift.String {
  case normal
  case slow
  case verySlow
  public init?(rawValue: Swift.String)
  public typealias RawValue = Swift.String
  public var rawValue: Swift.String {
    get
  }
}
@objc public class PlotViewController : ObjectiveC.NSObject, CorePlot.CPTPlotDataSource {
  public init(view: CorePlot.CPTGraphHostingView, hasDarkTheme: Swift.Bool = false)
  @objc public func numberOfRecords(for plot: CorePlot.CPTPlot) -> Swift.UInt
  @objc public func double(for plot: CorePlot.CPTPlot, field fieldEnum: Swift.UInt, record idx: Swift.UInt) -> Swift.Double
  public func appendToPlot(_ value: Swift.Double)
  @objc deinit
}
public protocol VerificationListener : ObjectiveC.NSObjectProtocol {
  func verificationResult(isVerified: Swift.Bool, error: Swift.String)
}
public let AyuSynk: AyuSynkSdk.SdkApiManager
@_hasMissingDesignatedInitializers public class MurataADPCMEngine {
  @objc deinit
}
public enum DeviceConnectionState {
  case DEVICE_CONNECTED
  case DEVICE_DISCONNECTED
  public static func == (a: AyuSynkSdk.DeviceConnectionState, b: AyuSynkSdk.DeviceConnectionState) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
public enum DeviceStrength {
  case DEVICE_SIGNAL_WEAK
  case DEVICE_SIGNAL_STRONG
  public static func == (a: AyuSynkSdk.DeviceStrength, b: AyuSynkSdk.DeviceStrength) -> Swift.Bool
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
@objc @_inheritsConvenienceInitializers @_hasMissingDesignatedInitializers @IBDesignable @_Concurrency.MainActor(unsafe) public class BreathingGuideView : UIKit.UIView {
  @_Concurrency.MainActor(unsafe) @objc override dynamic public init(frame: CoreFoundation.CGRect)
  @_Concurrency.MainActor(unsafe) @objc override dynamic public func awakeFromNib()
  @_Concurrency.MainActor(unsafe) public func startBreathingAnimation()
  @_Concurrency.MainActor(unsafe) public func stopBreathingAnimation()
  @_Concurrency.MainActor(unsafe) public func setOuterBorderColor(_ color: UIKit.UIColor)
  @_Concurrency.MainActor(unsafe) public func setInnerCircleColor(_ color: UIKit.UIColor)
  @_Concurrency.MainActor(unsafe) public func setFontSize(_ fontSize: CoreFoundation.CGFloat)
  @_Concurrency.MainActor(unsafe) public func setTextColor(_ color: UIKit.UIColor)
  @objc deinit
}
public protocol AyuDeviceListener : ObjectiveC.NSObjectProtocol {
  func deviceConnectionStrength(strength: AyuSynkSdk.DeviceStrength)
  func deviceConnectionState(state: AyuSynkSdk.DeviceConnectionState)
  func deviceBattery(battery: Swift.Int)
  func deviceConnectionFailed(error: Swift.String)
}
public protocol DeviceScanListener : ObjectiveC.NSObjectProtocol {
  func onScanStart()
  func onDeviceFound(device: AyuSynkSdk.Device)
  func onScanFinish()
  func onScanFailed(error: Swift.String)
}
public struct Device {
  public var name: Swift.String
  public var uniqueIdentifier: Swift.String
}
@objc @_inheritsConvenienceInitializers @_hasMissingDesignatedInitializers public class SdkApiManager : ObjectiveC.NSObject {
  public static let `default`: AyuSynkSdk.SdkApiManager
  @objc deinit
}
extension AyuSynkSdk.SdkApiManager {
  public func getVersion() -> Swift.String
  public func verify(clientId: Swift.String, verificationListener: (any AyuSynkSdk.VerificationListener)?)
  public func setAyuDeviceListener(listener: any AyuSynkSdk.AyuDeviceListener)
  public func setDeviceScanListener(listener: any AyuSynkSdk.DeviceScanListener)
  public func isBluetoothEnabled() -> Swift.Bool
  public func startScan()
  public func stopScan()
  public func setAutoConnect(autoConnect: Swift.Bool)
  public func connect(deviceUUID: Swift.String)
  public func disconnect()
  public func isDeviceConnected() -> AyuSynkSdk.DeviceConnectionState
  public func getDeviceStrength() -> AyuSynkSdk.DeviceStrength
  public func getCurrentBatteryLevel() -> Swift.Int
  public func setRecorderListener(listener: any AyuSynkSdk.RecorderListener)
  public func changePlaybackSpeed(speed: AyuSynkSdk.PlayBackSpeeds)
  public func resetSpeed()
  public func setRecordingTimeLimit(recordingTimeLimit: Swift.Int)
  public func startRecording()
  public func pauseRecording()
  public func muteAudio(mute: Swift.Bool)
  public func clearRecordedData()
  public func getAudioData(recordID: Swift.String) -> Foundation.Data?
  public func playAudio(recordID: Swift.String)
  public func stopAudioPlayback()
  public func generateFile(fileName: Swift.String, recordID: Swift.String) -> Foundation.URL?
  public func generateFile(fileName: Swift.String, sampleData: Foundation.Data) -> Foundation.URL?
  public func setupVisualizerView(view: CorePlot.CPTGraphHostingView, color: CoreGraphics.CGColor)
  public func setDiagnosisReportUpdateListener(listener: any AyuSynkSdk.DiagnosisReportUpdateListener)
  public func generateDiagnosisReport(soundFile: AyuSynkSdk.SoundFile)
  public func setFilter(filter: AyuSynkSdk.FilterType)
  public func close()
}
extension AyuSynkSdk.SdkApiManager {
  public func reportRequestAdded(soundFile: AyuSynkSdk.SoundFile)
  public func reportGenerated(soundFile: AyuSynkSdk.SoundFile)
  public func onReportGenerationError(error: Swift.String?)
}
extension AyuSynkSdk.FilterType : Swift.Equatable {}
extension AyuSynkSdk.FilterType : Swift.Hashable {}
extension AyuSynkSdk.FilterType : Swift.RawRepresentable {}
extension AyuSynkSdk.SoundType : Swift.Equatable {}
extension AyuSynkSdk.SoundType : Swift.Hashable {}
extension AyuSynkSdk.LocationType : Swift.Equatable {}
extension AyuSynkSdk.LocationType : Swift.Hashable {}
extension AyuSynkSdk.LocationType : Swift.RawRepresentable {}
extension AyuSynkSdk.LocationType.Heart : Swift.Equatable {}
extension AyuSynkSdk.LocationType.Heart : Swift.Hashable {}
extension AyuSynkSdk.LocationType.Heart : Swift.RawRepresentable {}
extension AyuSynkSdk.LocationType.Lung : Swift.Equatable {}
extension AyuSynkSdk.LocationType.Lung : Swift.Hashable {}
extension AyuSynkSdk.LocationType.Lung : Swift.RawRepresentable {}
extension AyuSynkSdk.PlayBackSpeeds : Swift.Equatable {}
extension AyuSynkSdk.PlayBackSpeeds : Swift.Hashable {}
extension AyuSynkSdk.PlayBackSpeeds : Swift.RawRepresentable {}
extension AyuSynkSdk.DeviceConnectionState : Swift.Equatable {}
extension AyuSynkSdk.DeviceConnectionState : Swift.Hashable {}
extension AyuSynkSdk.DeviceStrength : Swift.Equatable {}
extension AyuSynkSdk.DeviceStrength : Swift.Hashable {}
extension AyuSynkSdk.SdkApiManager : AyuSynkSdk.DiagnosisReportUpdateListener {}
