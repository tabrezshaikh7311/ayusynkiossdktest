#ifndef FILTER_CONFIG_H
#define FILTER_CONFIG_H

#ifdef __cplusplus
extern "C" {
#endif
	
#define FILTER_DEBUG					0u
#define OFFLINE							0u

#define SAMPLING_FREQ					(8000u)
#define SAMPLES_PER_MS					(SAMPLING_FREQ / 1000u)

#define DEC_FACTOR						(4u)

#if OFFLINE == 1u
#define PCM_SAMPLES_IN_MS				(60000u)				// change it based on pcm samples length in file						
#else
#define PCM_SAMPLES_IN_MS				(10u)	
#endif

#define PCM_SAMPLES_IN					(PCM_SAMPLES_IN_MS * SAMPLES_PER_MS)

#if OFFLINE == 1u
#define BLOCK_TIME_MS					(PCM_SAMPLES_IN_MS)			
#define OVERLAP_TIME_MS					(0u)
#else
#define BLOCK_TIME_MS					(150u)
#define OVERLAP_TIME_MS					(330u)
#endif


#define OVERLAP_SIZE          			((OVERLAP_TIME_MS * SAMPLES_PER_MS) / DEC_FACTOR)	
#define BLOCK_SIZE		        		((BLOCK_TIME_MS * SAMPLES_PER_MS) / DEC_FACTOR)
#define OVERLAP_BLOCK_SIZE				(OVERLAP_SIZE * 2)

#define EMD_PROCESS_BLOCK_SIZE			(BLOCK_SIZE + OVERLAP_BLOCK_SIZE)
#define CIRCULAR_BUFFER_SIZE			((BLOCK_SIZE * 2) + OVERLAP_BLOCK_SIZE)

#define FILTER_BLOCK_C_SIZE				(BLOCK_SIZE * DEC_FACTOR)

#define PCM_SAMPLES_OUT					(FILTER_BLOCK_C_SIZE)

#ifdef __cplusplus
}
#endif

#endif
