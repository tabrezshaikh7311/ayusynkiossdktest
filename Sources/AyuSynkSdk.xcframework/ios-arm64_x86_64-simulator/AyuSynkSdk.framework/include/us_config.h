#ifndef US_CONFIG_H
#define US_CONFIG_H

#ifdef __cplusplus
extern "C" {
#endif

#include "arm_math.h"

#define IN_SAMPLING_FREQ						(8000u)
#define IN_SAMPLES_PER_MS					    (IN_SAMPLING_FREQ / 1000u)

#define UPSAMPLING_FREQ                         (48000u)
#define UPSAMPLE_FACTOR						    (UPSAMPLING_FREQ / IN_SAMPLING_FREQ)

#define UP_SAMPLER_IN_FRAME_MS				    (20u)									
#define UP_SAMPLER_IN_FRAME_LEN				    (UP_SAMPLER_IN_FRAME_MS * IN_SAMPLES_PER_MS)

#define UP_SAMPLER_OUT_FRAME_LEN				(UP_SAMPLER_IN_FRAME_LEN * UPSAMPLE_FACTOR)

int up_sampler_process_block(int16_t *pcm_in, int16_t *pcm_out);

#ifdef __cplusplus
}
#endif

#endif
