#ifndef A3_BLOCK_H
#define A3_BLOCK_H

#include "filters.h"

#define a3b_1_size					(BLOCK_SIZE)
#define n_bin						(100u)
#define a3b_1_buffer_len_sec		1.5f
#define a3b_1_buffer_len			(int) (a3b_1_buffer_len_sec * (SAMPLING_FREQ / DEC_FACTOR))     
#define a3b_1_bin_size				(int) (a3b_1_buffer_len / n_bin)
#define a3b_1_buff1_len				(int) (a3b_1_size)		

#define a3b_2_size					(BLOCK_SIZE)
#define a3b_2_buffer_len_sec		1.0f
#define a3b_2_buffer_len			(int) (a3b_2_buffer_len_sec * (SAMPLING_FREQ / DEC_FACTOR))     
#define a3b_2_bin_size				(int) (a3b_2_buffer_len / n_bin)
#define a3b_2_buff1_len				(int) (a3b_2_size)		

#define a3b_buff1_len				a3b_2_buff1_len  // assign whichever is the highest of block_size and pcm_samples_in

typedef struct A3_INST {
	float32_t* buffer;
	int buff_len; 
	int buff_idx;
	int bin_size;
	float32_t* max_buff;
	float32_t g1;
	float32_t g2;
}a3b_inst;

void filter_init_a3b(void);
uint32_t filter_a3b_process(float32_t* data, float32_t* amp_data, uint32_t nsample);
	
#endif // A3_BLOCK_H
