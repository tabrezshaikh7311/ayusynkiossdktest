#ifndef FILTER_WRAPPER_H
#define FILTER_WRAPPER_H

#ifdef __cplusplus
extern "C" {
#endif
	
#include <arm_math.h>
#include <math.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdio.h>
#include "filters.h"

void filter_init(int mode);

int16_t applycmsisfilter(int16_t *data_in, int16_t *data_out);

#ifdef __cplusplus
}
#endif
	
#endif // FILTER_WRAPPER_H
