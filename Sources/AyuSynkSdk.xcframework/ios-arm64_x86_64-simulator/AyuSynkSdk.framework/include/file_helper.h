#ifndef FILE_HELPER_H
#define FILE_HELPER_H

#include <stdio.h>
#include <stdint.h>
#include <stdbool.h>

typedef enum
{
    mode_rb = 0,
    mode_wb_p
} file_open_mode_t;

typedef struct
{
    uint8_t name[255];
    FILE *p_fstream;
    uint32_t data_written;
} file_t;

bool open_file(file_t *p_file, file_open_mode_t mode);
bool close_file(file_t *p_file);
bool read_file(file_t *p_file, int16_t *data, uint32_t len);
bool write_file(file_t *p_file, int16_t *data, uint32_t len);
bool write_file_float32_t(file_t* p_file, float* data, uint32_t len);
uint32_t get_wav_file_length(file_t *p_file);

#endif
