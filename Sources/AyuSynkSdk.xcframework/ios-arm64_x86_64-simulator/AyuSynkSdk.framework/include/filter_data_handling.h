#ifndef __FILTERB_DH_H
#define __FILTERB_DH_H

#include "filters.h"
extern float32_t filterB_prcess_buffer[];

void filterBlockB_cbuff_init(void);
void filterBlockB_cbuff_reset(void);
uint32_t push_pcm_samples_to_cbuff(float* src_buff, uint32_t len);
void process_filled_buffer(void);
void filterB_data_handling(void);
void extract_filtered_samples(float* out_buff);
void enable_size_match_notification(uint32_t size);

#endif
